import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.unsplash.com/',
    headers: {
        Authorization: 'Client-ID 5fd7514cf141a216e4244c55ec50dd930e2c563d494d6c5d2220a2b033c0425b'
    }
});